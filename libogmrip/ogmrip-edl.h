/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_EDL_H__
#define __OGMRIP_EDL_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * OGMRipEdlAction:
 * @OGMRIP_EDL_ACTION_SKIP: The skip action
 * @OGMRIP_EDL_ACTION_MUTE: The mute action
 *
 * The available actions of a EDL action.
 */
typedef enum
{
  OGMRIP_EDL_ACTION_SKIP,
  OGMRIP_EDL_ACTION_MUTE
} OGMRipEdlAction;

/**
 * OGMRipEdlFunc:
 * @action: an #OGMRipEdlAction
 * @start: the start in seconds
 * @end: the end in seconds
 * @data: the user data
 *
 * Specifies the type of functions passed to ogmrip_edl_foreach().
 */
typedef void (* OGMRipEdlFunc) (OGMRipEdlAction action,
                                gdouble         start,
                                gdouble         end,
                                gpointer        data);

typedef struct _OGMRipEdl OGMRipEdl;

OGMRipEdl *   ogmrip_edl_new             (const gchar     *filename);
void          ogmrip_edl_ref             (OGMRipEdl       *edl);
void          ogmrip_edl_unref           (OGMRipEdl       *edl);

const gchar * ogmrip_edl_get_filename    (OGMRipEdl       *edl);

void          ogmrip_edl_add             (OGMRipEdl       *edl,
                                          OGMRipEdlAction action,
                                          gdouble         start,
                                          gdouble         end);
void          ogmrip_edl_foreach         (OGMRipEdl       *edl,
                                          OGMRipEdlFunc   func,
                                          gpointer        data);

gboolean      ogmrip_edl_dump            (OGMRipEdl       *edl);

G_END_DECLS

#endif /* __OGMRIP_FS_H__ */

