/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_VIDEO_CODEC_H__
#define __OGMRIP_VIDEO_CODEC_H__

#include <ogmrip-codec.h>
#include <ogmrip-enums.h>

G_BEGIN_DECLS

#define OGMRIP_TYPE_VIDEO_CODEC           (ogmrip_video_codec_get_type ())
#define OGMRIP_VIDEO_CODEC(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), OGMRIP_TYPE_VIDEO_CODEC, OGMRipVideoCodec))
#define OGMRIP_VIDEO_CODEC_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST ((klass), OGMRIP_TYPE_VIDEO_CODEC, OGMRipVideoCodecClass))
#define OGMRIP_IS_VIDEO_CODEC(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), OGMRIP_TYPE_VIDEO_CODEC))
#define OGMRIP_IS_VIDEO_CODEC_CLASS(obj)  (G_TYPE_CHECK_CLASS_TYPE ((klass), OGMRIP_TYPE_VIDEO_CODEC))
#define OGMRIP_VIDEO_CODEC_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), OGMRIP_TYPE_VIDEO_CODEC, OGMRipVideoCodecClass))

typedef struct _OGMRipVideoCodec      OGMRipVideoCodec;
typedef struct _OGMRipVideoCodecPriv  OGMRipVideoCodecPriv;
typedef struct _OGMRipVideoCodecClass OGMRipVideoCodecClass;

struct _OGMRipVideoCodec
{
  OGMRipCodec parent_instance;

  OGMRipVideoCodecPriv *priv;
};

struct _OGMRipVideoCodecClass
{
  OGMRipCodecClass parent_class;

  /* signals */
  void (* pass) (OGMRipVideoCodec *video,
                 guint       pass);
  
  /* vtable */
  gint (* get_start_delay) (OGMRipVideoCodec  *video);
  void (* set_quality)     (OGMRipVideoCodec  *video,
                            OGMRipQualityType quality);
};

GType     ogmrip_video_codec_get_type             (void);

void      ogmrip_video_codec_set_angle            (OGMRipVideoCodec  *video,
                                                   guint             angle);
gint      ogmrip_video_codec_get_angle            (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_bitrate          (OGMRipVideoCodec  *video,
                                                   guint             bitrate);
gint      ogmrip_video_codec_get_bitrate          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_quantizer        (OGMRipVideoCodec  *video,
                                                   gdouble           quantizer);
gdouble   ogmrip_video_codec_get_quantizer        (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_bits_per_pixel   (OGMRipVideoCodec  *video,
                                                   gdouble           bpp);
gdouble   ogmrip_video_codec_get_bits_per_pixel   (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_passes           (OGMRipVideoCodec  *video,
                                                   guint             pass);
gint      ogmrip_video_codec_get_passes           (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_threads          (OGMRipVideoCodec  *video,
                                                   guint             threads);
gint      ogmrip_video_codec_get_threads          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_scaler           (OGMRipVideoCodec  *video,
                                                   OGMRipScalerType  scaler);
gint      ogmrip_video_codec_get_scaler           (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_deinterlacer     (OGMRipVideoCodec  *video,
                                                   OGMRipDeintType   deint);
gint      ogmrip_video_codec_get_deinterlacer     (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_trellis          (OGMRipVideoCodec  *video,
                                                   gboolean          trellis);
gboolean  ogmrip_video_codec_get_trellis          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_4mv              (OGMRipVideoCodec  *video,
                                                   gboolean          v4mv);
gboolean  ogmrip_video_codec_get_4mv              (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_qpel             (OGMRipVideoCodec  *video,
                                                   gboolean          qpel);
gboolean  ogmrip_video_codec_get_qpel             (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_turbo            (OGMRipVideoCodec  *video,
                                                   gboolean          turbo);
gboolean  ogmrip_video_codec_get_turbo            (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_grayscale        (OGMRipVideoCodec  *video,
                                                   gboolean          grayscale);
gboolean  ogmrip_video_codec_get_grayscale        (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_cartoon          (OGMRipVideoCodec  *video,
                                                   gboolean          cartoon);
gboolean  ogmrip_video_codec_get_cartoon          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_denoise          (OGMRipVideoCodec  *video,
                                                   gboolean          denoise);
gboolean  ogmrip_video_codec_get_denoise          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_max_b_frames     (OGMRipVideoCodec  *video,
                                                   guint             max_b_frames);
gint      ogmrip_video_codec_get_max_b_frames     (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_quality          (OGMRipVideoCodec  *video,
                                                   OGMRipQualityType quality);
gint      ogmrip_video_codec_get_quality          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_deblock          (OGMRipVideoCodec  *video,
                                                   gboolean          deblock);
gboolean  ogmrip_video_codec_get_deblock          (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_set_dering           (OGMRipVideoCodec  *video,
                                                   gboolean          dering);
gboolean  ogmrip_video_codec_get_dering           (OGMRipVideoCodec  *video);
gint      ogmrip_video_codec_get_start_delay      (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_get_raw_size         (OGMRipVideoCodec  *video,
                                                   guint             *width,
                                                   guint             *height);
gboolean  ogmrip_video_codec_get_crop_size        (OGMRipVideoCodec  *video,
                                                   guint             *x,
                                                   guint             *y,
                                                   guint             *width,
                                                   guint             *height);
void      ogmrip_video_codec_set_crop_size        (OGMRipVideoCodec  *video,
                                                   guint             x,
                                                   guint             y,
                                                   guint             width,
                                                   guint             height);
gboolean  ogmrip_video_codec_get_scale_size       (OGMRipVideoCodec  *video,
                                                   guint             *width,
                                                   guint             *height);
void      ogmrip_video_codec_set_scale_size       (OGMRipVideoCodec  *video,
                                                   guint             width,
                                                   guint             height);
gboolean  ogmrip_video_codec_get_max_size         (OGMRipVideoCodec  *video,
                                                   guint             *width,
                                                   guint             *height,
                                                   gboolean          *expand);
void      ogmrip_video_codec_set_max_size         (OGMRipVideoCodec  *video,
                                                   guint             width,
                                                   guint             height,
                                                   gboolean          expand);
gboolean  ogmrip_video_codec_get_min_size         (OGMRipVideoCodec  *video,
                                                   guint             *width,
                                                   guint             *height);
void      ogmrip_video_codec_set_min_size         (OGMRipVideoCodec  *video,
                                                   guint             width,
                                                   guint             height);
void      ogmrip_video_codec_get_aspect_ratio     (OGMRipVideoCodec  *video,
                                                   guint             *num,
                                                   guint             *denom);
void      ogmrip_video_codec_set_aspect_ratio     (OGMRipVideoCodec  *video,
                                                   guint             num,
                                                   guint             denom);
gboolean  ogmrip_video_codec_analyze              (OGMRipVideoCodec  *video,
                                                   guint             nframes);
gboolean  ogmrip_video_codec_autocrop             (OGMRipVideoCodec  *video,
                                                   guint             nframes);
void      ogmrip_video_codec_autoscale            (OGMRipVideoCodec  *video);
void      ogmrip_video_codec_autobitrate          (OGMRipVideoCodec  *video, 
                                                   guint64           nonvideo_size,
                                                   guint64           overhead_size,
                                                   guint64           total_size);
gint      ogmrip_video_codec_is_interlaced        (OGMRipVideoCodec  *video);

OGMDvdAudioStream * ogmrip_video_codec_get_ensure_sync (OGMRipVideoCodec  *video);
void                ogmrip_video_codec_set_ensure_sync (OGMRipVideoCodec  *video,
                                                        OGMDvdAudioStream *stream);

OGMDvdSubpStream *  ogmrip_video_codec_get_hard_subp   (OGMRipVideoCodec  *video,
                                                        gboolean          *forced);
void                ogmrip_video_codec_set_hard_subp   (OGMRipVideoCodec  *video,
                                                        OGMDvdSubpStream  *stream,
                                                        gboolean          forced);

G_END_DECLS

#endif /* __OGMRIP_VIDEO_CODEC_H__ */

