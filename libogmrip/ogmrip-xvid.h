/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_XVID_H__
#define __OGMRIP_XVID_H__

#include <ogmrip-codec.h>

G_BEGIN_DECLS

#define OGMRIP_XVID_SECTION                  "xvid"

#define OGMRIP_XVID_PROP_B_ADAPT             "b_adapt"
#define OGMRIP_XVID_PROP_BQUANT_OFFSET       "bquant_offset"
#define OGMRIP_XVID_PROP_BQUANT_RATIO        "bquant_ratio"
#define OGMRIP_XVID_PROP_BVHQ                "bvhq"
#define OGMRIP_XVID_PROP_CHROMA_ME           "chroma_me"
#define OGMRIP_XVID_PROP_CHROMA_OPT          "chroma_opt"
#define OGMRIP_XVID_PROP_CLOSED_GOP          "closed_gop"
#define OGMRIP_XVID_PROP_FRAME_DROP_RATIO    "frame_drop_ratio"
#define OGMRIP_XVID_PROP_GMC                 "gmc"
#define OGMRIP_XVID_PROP_INTERLACING         "interlacing"
#define OGMRIP_XVID_PROP_MAX_BQUANT          "max_bquant"
#define OGMRIP_XVID_PROP_MAX_IQUANT          "max_iquant"
#define OGMRIP_XVID_PROP_MAX_PQUANT          "max_pquant"
#define OGMRIP_XVID_PROP_ME_QUALITY          "me_quality"
#define OGMRIP_XVID_PROP_MIN_BQUANT          "min_bquant"
#define OGMRIP_XVID_PROP_MIN_IQUANT          "min_iquant"
#define OGMRIP_XVID_PROP_MIN_PQUANT          "min_pquant"
#define OGMRIP_XVID_PROP_MAX_KEYINT          "max_key_interval"
#define OGMRIP_XVID_PROP_PACKED              "packed"
#define OGMRIP_XVID_PROP_PAR_HEIGHT          "par_height"
#define OGMRIP_XVID_PROP_PAR                 "par"
#define OGMRIP_XVID_PROP_PAR_WIDTH           "par_width"
#define OGMRIP_XVID_PROP_PROFILE             "profile"
#define OGMRIP_XVID_PROP_QUANT_TYPE          "quant_type"
#define OGMRIP_XVID_PROP_VHQ                 "vhq"

#define OGMRIP_XVID_PROP_BFRAMES             "bframes"

#define OGMRIP_XVID_DEFAULT_B_ADAPT          FALSE
#define OGMRIP_XVID_DEFAULT_BQUANT_OFFSET    100
#define OGMRIP_XVID_DEFAULT_BQUANT_RATIO     150
#define OGMRIP_XVID_DEFAULT_BVHQ             1
#define OGMRIP_XVID_DEFAULT_CHROMA_ME        TRUE
#define OGMRIP_XVID_DEFAULT_CHROMA_OPT       TRUE
#define OGMRIP_XVID_DEFAULT_CLOSED_GOP       TRUE
#define OGMRIP_XVID_DEFAULT_FRAME_DROP_RATIO 0
#define OGMRIP_XVID_DEFAULT_GMC              FALSE
#define OGMRIP_XVID_DEFAULT_INTERLACING      FALSE
#define OGMRIP_XVID_DEFAULT_MAX_BQUANT       31
#define OGMRIP_XVID_DEFAULT_MAX_IQUANT       31
#define OGMRIP_XVID_DEFAULT_MAX_PQUANT       31
#define OGMRIP_XVID_DEFAULT_ME_QUALITY       6
#define OGMRIP_XVID_DEFAULT_MIN_BQUANT       2
#define OGMRIP_XVID_DEFAULT_MIN_IQUANT       2
#define OGMRIP_XVID_DEFAULT_MIN_PQUANT       2
#define OGMRIP_XVID_DEFAULT_MAX_KEYINT       250
#define OGMRIP_XVID_DEFAULT_PACKED           FALSE
#define OGMRIP_XVID_DEFAULT_PAR              0
#define OGMRIP_XVID_DEFAULT_PAR_HEIGHT       1
#define OGMRIP_XVID_DEFAULT_PAR_WIDTH        1
#define OGMRIP_XVID_DEFAULT_PROFILE          0
#define OGMRIP_XVID_DEFAULT_QUANT_TYPE       0
#define OGMRIP_XVID_DEFAULT_VHQ              1

#define OGMRIP_XVID_DEFAULT_4MV              TRUE
#define OGMRIP_XVID_DEFAULT_BFRAMES          2
#define OGMRIP_XVID_DEFAULT_TRELLIS          TRUE

#define OGMRIP_TYPE_XVID (ogmrip_xvid_get_type ())

GType ogmrip_xvid_get_type (void);

G_END_DECLS

#endif /* __OGMRIP_XVID_H__ */

