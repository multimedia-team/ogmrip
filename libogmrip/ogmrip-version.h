#ifndef __OGMRIP_VERSION_H__
#define __OGMRIP_VERSION_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * OGMRIP_MAJOR_VERSION:
 *
 * Compile time major version of OGMRip
 */
#define OGMRIP_MAJOR_VERSION       (1)

/**
 * OGMRIP_MINOR_VERSION:
 *
 * Compile time minor version of OGMRip
 */
#define OGMRIP_MINOR_VERSION       (0)

/**
 * OGMRIP_MICRO_VERSION:
 *
 * Compile time micro version of OGMRip
 */
#define OGMRIP_MICRO_VERSION       (1)

/**
 * OGMRIP_CHECK_VERSION:
 * @major: A major version number
 * @minor: A minor version number
 * @micro: A micro version number
 *
 * Checks whether version is equal or greather than major.minor.micro
 */

#define OGMRIP_CHECK_VERSION(major,minor,micro)  \
  ((OGMRIP_MAJOR_VERSION > (major)) || \
   (OGMRIP_MAJOR_VERSION == (major) && OGMRIP_MINOR_VERSION > (minor)) || \
   (OGMRIP_MAJOR_VERSION == (major) && OGMRIP_MINOR_VERSION == (minor) && OGMRIP_MICRO_VERSION >= (micro)))

/**
 * MPLAYER_CHECK_VERSION:
 * @major: A major version number
 * @minor: A minor version number
 * @rc: An rc version number
 * @pre: A pre version number
 *
 * Check if version is equal or greather than major.minor, major.minor-rc or
 * major.minor-pre, in that order
 */
#define MPLAYER_CHECK_VERSION(major,minor,rc,pre) \
  (ogmrip_check_mplayer_version (major, minor, rc, pre))

gboolean ogmrip_check_mplayer         (void);
gboolean ogmrip_check_mencoder        (void);

gboolean ogmrip_check_mplayer_version (gint major,
                                       gint minor,
                                       gint rc,
                                       gint pre);
gboolean ogmrip_check_mplayer_dts     (void);
gboolean ogmrip_check_mplayer_nosub   (void);

G_END_DECLS

#endif /* __OGMRIP_VERSION_H__ */

