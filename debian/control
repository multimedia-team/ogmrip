Source: ogmrip
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Rico Tzschichholz <ricotz@ubuntu.com>
Build-Depends:
 debhelper-compat (= 13),
 gtk-doc-tools,
 intltool,
 libdvdread-dev,
 libenca-dev,
 libenchant-2-dev,
 libnotify-dev,
 libtheora-dev,
 libtiff-dev,
 libtool,
 libvorbis-dev,
 libxml2-dev,
 libxml-parser-perl,
 zlib1g-dev
Standards-Version: 4.6.2
Homepage: https://ogmrip.sourceforge.net
Vcs-Git: https://salsa.debian.org/multimedia-team/ogmrip.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/ogmrip
Rules-Requires-Root: no

Package: ogmrip
Architecture: any
Depends:
 lame,
 mencoder,
 mkvtoolnix,
 mplayer-nogui | mplayer,
 ogmrip-plugins (= ${binary:Version}),
 ogmtools,
 tesseract-ocr | ocrad | gocr,
 vorbis-tools,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 ogmrip-ac3,
 ogmrip-dirac,
 ogmrip-doc,
 ogmrip-mpeg,
 ogmrip-oggz,
 ogmrip-profiles,
 ogmrip-video-copy
Replaces: libogmrip0
Conflicts: libogmrip0
Description: Application for ripping and encoding DVD
 ogmrip is an application and a set of libraries for ripping and encoding
 DVD into AVI, OGM MP4 or Matroska files using a wide variety of codecs. It
 relies on mplayer, mencoder, ogmtools, mkvtoolnix, oggenc, lame and faac to
 perform its tasks.
  o transcodes from DVD or files
  o outputs ogm, avi, mp4 or matroska files
  o calculates video bitrate for a given filesize
  o calculates cropping parameters and scaling factors
  o supports multiple audio and subtitles streams encoding
  o lots of codecs (vorbis, mp3, pcm, ac3, dts, aac, xvid, lavc, x264, theora)
  o uses maximum quality codec switches
  o rips contiguous chapters

Package: ogmrip-plugins
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Enhances: ogmrip
Description: Application for ripping and encoding DVD - plugins
 ogmrip is an application and a set of libraries for ripping and encoding
 DVD into AVI, OGM MP4 or Matroska files using a wide variety of codecs. It
 relies on mplayer, mencoder, ogmtools, mkvtoolnix, oggenc, lame and faac to
 perform its tasks.
 .
 This package provides the plugins set for OGMRip.

Package: ogmrip-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Application for ripping and encoding DVD - Documentation files
 ogmrip is an application and a set of libraries for ripping and encoding
 DVD into AVI, OGM MP4 or Matroska files using a wide variety of codecs. It
 relies on mplayer, mencoder, ogmtools, mkvtoolnix, oggenc, lame and faac to
 perform its tasks.
  o transcodes from DVD or files
  o outputs ogm, avi, mp4 or matroska files
  o calculates video bitrate for a given filesize
  o calculates cropping parameters and scaling factors
  o supports multiple audio and subtitles streams encoding
  o lots of codecs (vorbis, mp3, pcm, ac3, dts, aac, xvid, lavc, x264, theora)
  o uses maximum quality codec switches
  o rips contiguous chapters
 .
 This package contains the documentation files.

Package: libogmrip1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Application for ripping and encoding DVD - libraries files
 ogmrip is an application and a set of libraries for ripping and encoding
 DVD into AVI, OGM MP4 or Matroska files using a wide variety of codecs. It
 relies on mplayer, mencoder, ogmtools, mkvtoolnix, oggenc, lame and faac to
 perform its tasks.
  o transcodes from DVD or files
  o outputs ogm, avi, mp4 or matroska files
  o calculates video bitrate for a given filesize
  o calculates cropping parameters and scaling factors
  o supports multiple audio and subtitles streams encoding
  o lots of codecs (vorbis, mp3, pcm, ac3, dts, aac, xvid, lavc, x264, theora)
  o uses maximum quality codec switches
  o rips contiguous chapters
 .
 This package contains the shared libraries.

Package: libogmrip-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 intltool,
 libdvdread-dev,
 libenca-dev,
 libogmrip1 (= ${binary:Version}),
 ${misc:Depends}
Description: Application for ripping and encoding DVD - development files
 ogmrip is an application and a set of libraries for ripping and encoding
 DVD into AVI, OGM MP4 or Matroska files using a wide variety of codecs. It
 relies on mplayer, mencoder, ogmtools, mkvtoolnix, oggenc, lame and faac to
 perform its tasks.
  o transcodes from DVD or files
  o outputs ogm, avi, mp4 or matroska files
  o calculates video bitrate for a given filesize
  o calculates cropping parameters and scaling factors
  o supports multiple audio and subtitles streams encoding
  o lots of codecs (vorbis, mp3, pcm, ac3, dts, aac, xvid, lavc, x264, theora)
  o uses maximum quality codec switches
  o rips contiguous chapters
 .
 This package contains the headers and development libraries.
