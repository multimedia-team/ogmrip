/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __OGMRIP_HELPER_H__
#define __OGMRIP_HELPER_H__

#include <gtk/gtk.h>
#include <sys/types.h>
#include <glade/glade.h>

#include <ogmdvd.h>
#include <ogmrip-settings.h>

G_BEGIN_DECLS

#define GTK_BOX_CHILD(b) ((GtkBoxChild *) (b))

/*
 * GLib
 */

gchar * g_get_locale (gint category);

/*
 * GObject
 */

/**
 * g_signal_connect_while_alive:
 * @instance: the instance to connect to
 * @detailed_signal: a string of the form "signal-name::detail"
 * @c_handler: the #GCallback to connect
 * @alive: the instance to check for
 *
 * Connects a #GCallback function to a signal for a particular object automatically
 * disconnecting it when @alive is destroyed.
 *
 * Returns: the handler id
 */
#define g_signal_connect_while_alive(instance, detailed_signal, c_handler, alive) \
    g_signal_connect_data_while_alive ((instance), (detailed_signal), (c_handler), (alive), NULL, (GConnectFlags) 0)

/**
 * g_signal_connect_swapped_while_alive:
 * @instance: the instance to connect to
 * @detailed_signal: a string of the form "signal-name::detail"
 * @c_handler: the #GCallback to connect
 * @alive: the instance to check for
 *
 * Connects a #GCallback function to a signal for a particular object automatically
 * disconnecting it when @alive is destroyed.
 *
 * Returns: the handler id
 */
#define g_signal_connect_swapped_while_alive(instance, detailed_signal, c_handler, alive) \
    g_signal_connect_data_while_alive ((instance), (detailed_signal), (c_handler), (alive), NULL, G_CONNECT_SWAPPED)

gulong g_signal_connect_data_while_alive (gpointer       instance,
                                          const gchar    *detailed_signal,
                                          GCallback      c_handler,
                                          gpointer       alive,
                                          GClosureNotify destroy_data,
                                          GConnectFlags  connect_flags);

/*
 * Gtk+
 */

void        gtk_window_set_parent           (GtkWindow        *window, 
                                             GtkWindow        *parent);
void        gtk_window_set_icon_from_stock  (GtkWindow        *window,
                                             const gchar      *stock_id);
gint        gtk_radio_button_get_active     (GtkRadioButton   *radio);
void        gtk_radio_button_set_active     (GtkRadioButton   *radio, 
                                             guint            index);
gboolean    gtk_tree_model_iter_prev        (GtkTreeModel     *tree_model,
                                             GtkTreeIter      *iter);
void        gtk_label_set_int               (GtkLabel         *label,
                                             gint             value);
gint        gtk_label_get_int               (GtkLabel         *label);
GtkWidget * gtk_box_get_nth_child           (GtkBox           *box,
                                             gint             n);
void        gtk_table_append                (GtkTable         *table,
                                             GtkWidget        *widget,
                                             GtkAttachOptions xoptions,
                                             GtkAttachOptions yoptions,
                                             guint            xpadding,
                                             guint            ypadding);
void        gtk_dialog_set_response_visible (GtkDialog        *dialog,
                                             gint             response_id,
                                             gboolean         setting);
void        gtk_dialog_response_accept      (GtkDialog        *dialog);

/*
 * OGMRip
 */

GtkWidget * ogmrip_message_dialog_new (GtkWindow      *parent,
                                       GtkMessageType type,
                                       const gchar    *format,
                                       ...);
gint        ogmrip_message_dialog     (GtkWindow      *parent, 
                                       GtkMessageType type, 
                                       const gchar    *format, 
                                       ...);

/*
 * OGMDvd
 */

GtkWidget * ogmrip_load_dvd_dialog_new (GtkWindow   *parent,
                                        OGMDvdDisc  *disc,
                                        const gchar *name,
                                        gboolean    cancellable);

/*
 * Languages
 */

void        ogmrip_combo_box_languages_construct    (GtkComboBox *combo,
                                                     const gchar *default_text);

/*
 * Containers and Codecs
 */

void        ogmrip_combo_box_containers_construct   (GtkComboBox *combo);
void        ogmrip_combo_box_add_containers         (GtkComboBox *combo);
GType       ogmrip_combo_box_get_active_container   (GtkComboBox *combo);
void        ogmrip_combo_box_set_active_container   (GtkComboBox *combo,
                                                     const gchar *container);
void        ogmrip_combo_box_video_codecs_construct (GtkComboBox *combo);
void        ogmrip_combo_box_add_video_codecs       (GtkComboBox *combo,
                                                     GType       container);
GType       ogmrip_combo_box_get_active_video_codec (GtkComboBox *combo);
void        ogmrip_combo_box_set_active_video_codec (GtkComboBox *combo,
                                                     const gchar *codec);
void        ogmrip_combo_box_audio_codecs_construct (GtkComboBox *combo);
void        ogmrip_combo_box_add_audio_codecs       (GtkComboBox *combo,
                                                     GType       container);
GType       ogmrip_combo_box_get_active_audio_codec (GtkComboBox *combo);
void        ogmrip_combo_box_set_active_audio_codec (GtkComboBox *combo,
                                                     const gchar *codec);
void        ogmrip_combo_box_subp_codecs_construct  (GtkComboBox *combo);
void        ogmrip_combo_box_add_subp_codecs        (GtkComboBox *combo,
                                                     GType       container);
GType       ogmrip_combo_box_get_active_subp_codec  (GtkComboBox *combo);
void        ogmrip_combo_box_set_active_subp_codec  (GtkComboBox *combo,
                                                     const gchar *codec);

/*
 * Profiles
 */

const gchar * ogmrip_get_system_profiles_dir (void);
const gchar * ogmrip_get_user_profiles_dir   (void);

G_END_DECLS

#endif /* __OGMRIP_HELPER_H__ */

