
#ifndef __ogmjob_cclosure_marshal_MARSHAL_H__
#define __ogmjob_cclosure_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* INT:VOID (ogmjob-marshal.list:24) */
extern void ogmjob_cclosure_marshal_INT__VOID (GClosure     *closure,
                                               GValue       *return_value,
                                               guint         n_param_values,
                                               const GValue *param_values,
                                               gpointer      invocation_hint,
                                               gpointer      marshal_data);

G_END_DECLS

#endif /* __ogmjob_cclosure_marshal_MARSHAL_H__ */

