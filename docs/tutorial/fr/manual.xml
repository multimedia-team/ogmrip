<html>
  <title>OGMRip</title>
  <subtitle>Manuel Utilisateur</subtitle>
  <alternates>
    <alternate name="english">en/index.html</alternate>
  </alternates>
  <content>
    <p>OGMRip est un ensemble de bibliothèques et de logiciels spécialement conçus pour faciliter l'encodage de DVD. Ce document décrit l'utilisation de l'interface graphique.</p>
    <h2 id="introduction">Introduction</h2>
    <p>OGMRip permet de transcoder des DVD en très grande qualité et produit des fichiers AVI, Ogg Media, Matroska ou MP4. Il supporte un grand nombre de codecs vidéo (XviD, Lavc, X264 et Ogg Theora) et audio (Ogg Vorbis, MP3, PCM, AC3, DTS et AAC) et peut extraire les sous-titres aux formats SRT ou VobSub. OGMRip automatise le plus possible toutes les étapes de l'encodage: il détermine automatiquement le taux d'échantillonnage vidéo pour une taille de fichier donnée et détecte les paramètres de découpage et de redimensionnement de l'image. Il permet également d'intégrer plusieurs pistes audio et plusieurs sous-titres dans un même fichier et d'encoder seulement quelques chapitres d'un titre vidéo.</p>
    <p>L'interface graphique, quant à elle, respecte les indications <a href="http://developer.gnome.org/projects/gup/hig/">HIG</a>.</p>
    <p>OGMRip est divisé en plusieurs composants qui interagissent les uns avec les autres. Certains sont des bibliothèques, d'autres, des exécutables.  Certains sont absolument nécessaires, d'autres facultatifs.</p>
    <ul>
      <li>libogmdvd: une bibliothèque pour obtenir des informations sur les DVD vidéo</li>
      <li>libogmdvd-gtk: une biliothèque de widgets GTK+ pour les DVD vidéo</li>
      <li>libogmjob: une bibliothèque pour exécuter des processus</li>
      <li>libogmrip: une bibliothèque pour faciliter l'encodage des DVD</li>
      <li>libogmrip-gtk: une bibliothèque de widgets GTK+ spécifiques à OGMRip</li>
      <li>dvdcpy: un logiciel pour copier un DVD ou un titre d'un DVD sur le disque dur</li>
      <li>subptools: un logiciel pour manipuler des fichiers de sous-titres au format XML</li>
      <li>subp2pgm: un logiciel pour convertir un fichier VobSub en images au format PGM</li>
      <li>subp2png: un logiciel pour convertir un fichier VobSub en images au format PNG</li>
      <li>subp2tiff: un logiciel pour convertir un fichier VobSub en images au format TIFF</li>
      <li>theoraenc: un encodeur de video au format Ogg Theora</li>
      <li>ogmrip: l'interface graphique</li>
    </ul>
    <h2 id="installation">Installation</h2>
    <p>OGMRip est développé en C et nécessite un certain nombre de bibliothèques et de logiciels pour être correctement configuré et compilé. D'autres composants sont complètement optionnels et activeront certaines fonctionnalités d'OGMRip s'ils sont d'ores et déjà installés sur le système. Ces fonctionnalités peuvent être désactivées par configuration.</p>
    <h3 id="composants_requis">Composants requis</h3>
    <p>Les logiciels suivants sont absolument nécessaires pour construire OGMRip:</p>
    <ul>
      <li>glib &gt;= 2.14.0: la bibliothèque de l'API standard gtk+/gimp</li>
      <li>libxml &gt;= 2.0: une bibliothèque pour manipuler des fichiers xml</li>
      <li>dvdread &gt;= 0.9.4: une bibliothèque de bas niveau pour manipuler des DVD vidéo</li>
      <li>mplayer &gt;= 0.92: le lecteur multimédia sur lequel OGMRip est basé</li>
      <li>mencoder &gt;= 0.92: l'encodeur qui va avec MPlayer</li>
      <li>enca &gt;= 1.9: un bibliothèque pour détecter le jeu de caractères d'un fichier</li>
      <li>intltool &gt;= 0.35: un ensemble d'outils pour l'internationalisation</li>
      <li>pkgconfig &gt;= 0.12: un utilitaire pour gérer les options de compilation et d'édition de liens</li>
    </ul>
    <h3 id="composants_optionnels">Composants optionnels</h3>
    <p>Pour construire l'interface graphique:</p>
    <ul>
      <li>gtk+ &gt;= 2.12.0: la bibliothèque graphique GTK+</li>
      <li>gconf &gt;= 2.6.0: le système de configuration GNOME</li>
      <li>libglade &gt;= 2.5.0: une bibliothèque pour construire des interfaces graphiques à l'exécution</li>
    </ul>
    <p>Pour interfacer l'interface graphique avec DBus:</p>
    <ul>
      <li>dbus-glib &gt;= 0.7.2: un bus logiciel système pour permettre aux applications d'échanger des informations</li>
    </ul>
    <p>Pour activer le support du conteneur Ogg Media (OGM):</p>
    <ul>
      <li>ogmtools &gt;= 1.0: un ensemble d'outils pour créer, modifier et inspecter des fichiers Ogg Media</li>
    </ul>
    <p>Pour activer le support du conteneur Matroska (MKV):</p>
    <ul>
      <li>mkvtoolnix &gt;= 0.9.5: un ensemble d'outils pour créer, modifier et inspecter des fichiers Matroska</li>
    </ul>
    <p>Pour activer le support du conteneur MP4:</p>
    <ul>
      <li>gpac &gt;= 0.4.2: une implémentation du standard Mpeg-4</li>
    </ul>
    <p>Pour activer le support du codec video Ogg Theora:</p>
    <ul>
      <li>libtheora &gt;= 1.0alpha5: une bibliothèque pour encoder et décoder des vidéos au format Ogg Theora</li>
    </ul>
    <p>Pour activer le support du codec audio MP3:</p>
    <ul>
      <li>lame &gt;= 3.96: un codec MP3</li>
    </ul>
    <p>Pour activer le support du codec audio Ogg Vorbis:</p>
    <ul>
      <li>vorbistools &gt;= 1.0: un codec Ogg Vorbis</li>
    </ul>
    <p>Pour activer le support du codec audio AAC:</p>
    <ul>
      <li>faac &gt;= 1.24: un codec Mpeg-4 audio</li>
    </ul>
    <p>Pour activer le support des sous-titres au format SRT:</p>
    <ul>
      <li>gocr &gt;= 0.39, ocrad &gt;= 0.15 or tesseract &gt;= 2.0: un logiciel de reconnaissance optique de caractères</li>
    </ul>
    <p>Pour activer la vérification des sous-titres au format texte:</p>
    <ul>
      <li>enchant &gt;= 1.1: une bibliothèque pour la vérification orthographique</li>
    </ul>
    <p>Pour activer le support des notifications:</p>
    <ul>
      <li>libnotify &gt;= 0.4.3: la bibliothèque de notification</li>
    </ul>
    <p>Pour activer le support du codec vidéo X264:</p>
    <ul>
      <li>MPlayer doit avoir le support X264 activé</li>
    </ul>
    <p>Pour activer le support du son au format DTS:</p>
    <ul>
      <li>MPlayer doit avoir le support DTS activé</li>
    </ul>
    <p>Pour extraire les sous-titres au format PNG</p>
    <ul>
      <li>libpng: une bibliothèque pour manipuler les images au format PNG</li>
    </ul>
    <p>Pour extraire les sous-titres au format TIFF (pour tesseract)</p>
    <ul>
      <li>libtiff: une bibliothèque pour manipuler les images au format TIFF</li>
    </ul>
    <p>Pour générer la documentation:</p>
    <ul>
      <li>xsltproc: un processeur XSLT en ligne de commande qui fait partie de la libxslt</li>
    </ul>
    <h3 id="options_conf">Options de configuration</h3>
    <p>Par défaut, OGMRip détecte automatiquement les fonctionnalités qui doivent ou non être activée en fonction des logiciels et bibliothèques installés sur le système. Il est toutefois possible d'inhiber par configuration certaines de ses fonctionnalités.</p>
    <ul>
      <li>--enable-maintainer-mode: active le mode de débogage; OGMRip affichera des informations supplémentaires pour aider à la détection et à la correction des bugs;</li>
      <li>--enable-gtk-doc: active la génération de la documentation</li>
      <li>--disable-gtk-support: désactive la compilation de l'interface graphique; seule les bibliothèques seront construites</li>
      <li>--disable-dbus-support: désactive le support DBus</li>
      <li>--disable-enchant-support: désactive la correction orthographique des sous-titres au format texte</li>

      <li>--disable-aac-support: désactive le support du codec audio AAC</li>
      <li>--disable-lavc-support: désactive le support du codec vidéo Lavc</li>
      <li>--disable-lavf-support: désactive le support des conteneurs Lavf</li>
      <li>--disable-mkv-support: désactive le support du conteneur Matroska</li>
      <li>--disable-mp3-support: désactive le support du codec audio MP3</li>
      <li>--disable-ogm-support: désactive le support du conteneur Ogg Media</li>
      <li>--disable-srt-support: désactive le support des sous-titres au format SRT</li>
      <li>--disable-theora-support: désactive le support du codec vidéo Ogg Theora</li>
      <li>--disable-vorbis-support: désactive le support du codec audio Ogg Vorbis</li>
      <li>--disable-x264-support: désactive le support du codec vidéo X264</li>
      <li>--disable-xvid-support: désactive le support du codec vidéo XviD</li>
      <li>--disable-libnotify-support: désactive le support des notifications</li>
    </ul>
    <p>Certaines fonctionnalités peuvent également être paramètrées par configuration.</p>
    <ul>
      <li>--with-ocr=auto|gocr|ocrad|tesseract: cette option permet de spécifier le logiciel d'OCR à utiliser</li>
    </ul>
    <h2 id="preferences">Préférences</h2>
    <p>La fenêtre de préférences est divisée en deux onglets.</p>
    <h3 id="pref_generales">Générales</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-pref-general.png"><img src="shots/ogmrip-pref-general-small.png" style="border: 0pt none ;" alt="pref-general"/></a>Cet onglet rassemble toutes les options qui se rapportent à l'encodage dans son ensemble.</p>
    <h4>Chemins</h4>
    <ul>
      <li><i>Dossier de destination:</i> Le répertoire dans lequel le film, les sous-titres externes et éventuellement le fichier de log seront stockés au terme du rip.</li>
      <li><i>Nom du fichier:</i> Une liste de patrons possibles pour le nom du fichier de rip. L'extension est toujours celle associée au conteneur.</li>
    </ul>
    <h4>Langues préférées</h4>
    <ul>
      <li><i>Audio:</i> La première piste audio dans cette langue sera automatiquement sélectionné quand un DVD est chargé.</li>
      <li><i>Sous-titres:</i> Le premier sous-titres dans cette langue sera automatiquement sélectionné quand un DVD est chargé.</li>
      <li><i>Chapitres:</i> Cette option permet de définir la langue du chapitrage.</li>
    </ul>
    <h3 id="pref_avancees">Avancées</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-pref-advanced.png"><img src="shots/ogmrip-pref-advanced-small.png" style="border: 0pt none ;" alt="pref-advanced"/></a>Les options de cet onglet sont uniquement réservées aux utilisateurs confirmés.</p>
    <h4>Copie de DVD</h4>
    <ul>
      <li><i>Copie le DVD sur le disque avant l'encodage:</i> Le DVD est copié sur disque dur en préalable à l'encodage ce qui permet, au prix de beaucoup d'espace disque (entre 5 et 10 Go), d'accélérer notablement l'encodage.</li>
      <li><i>Après l'encodage:</i> Cette option permet de définir les actions à entreprendre à la fin de l'encodage concernant la copie du DVD. Il est ainsi possible de ѕupprimer la copie, de la conserver sur le disque dur, de la conserver sur le disque dur et de mettre à jour l'interface graphique pour qu'un prochain encodage se fasse à partir de la copie ou de demander à l'utilisateur.</li>
    </ul>
    <h4>Divers</h4>
    <ul>
      <li><i>Dossier temporaire:</i> Le répertoire dans lequel tous les fichiers temporaires sont stockés.</li>
      <li><i>Ne pas supprimer les fichiers temporaires:</i> Les fichiers temporaires ne sont pas supprimés et sont laissés dans le dossier temporaire. Cette option est uniquement nécessaire à fin de débogage.</li>
      <li><i>Enregistrement des sorties des commandes:</i> Les affichages de toutes les commandes lancées par OGMRip ainsi que les lignes de commande elles-mêmes sont stockés dans un fichier qui portera le même nom que le rip avec l'extension .log.</li>
    </ul>
    <h2 id="profils">Profils</h2>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-profiles.png"><img src="shots/ogmrip-profiles-small.png" style="border: 0pt none ;" alt="profiles"/></a>Pour simplifier les choix du conteneur, des codecs et surtout des options, OGMRip permet de gérer des profils d'encodage. Par défaut, OGMRip en propose un certain nombre pour des configurations relativement standards. Il est toutefois possible de modifier ces profils, de les copier, de les supprimer, de les renommer, d'en créer de nouveaux, de les importer et de les exporter.</p>
    <p>Les profils par défaut sont:</p>
    <ul>
      <li><i>PC, Haute Qualité:</i> Un encodage vidéo en X264 et audio en AAC, le tout dans un conteneur Matroska, avec des options privilégiant la qualité à la vitesse et pour une taille finale de 700 Mo.</li>
      <li><i>PC, Basse Qualité:</i> Un encodage vidéo en Mpeg4 et audio en Vorbis, le tout dans un conteneur Ogg Média, avec des options privilégiant la vitesse à la qualité et pour une taille finale de 700 Mo.</li>
      <li><i>Compatible Platine DivX:</i> Un encodage vidéo en XviD et audio en MP3, le tout dans un conteneur AVI, avec des options privilégiant la compatibilité avec les platines de salon et pour une taille finale de 700 Mo.</li>
    </ul>
    <p>Des profils supplémentaires sont disponibles sur <a href="profiles.html">le site web d'OGMRip</a>.</p>
    <p>Les sections suivantes décrivent la fenêtre d'édition de profil.</p>
    <h3 id="profil_general">Général</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-profile-general.png"><img src="shots/ogmrip-profile-general-small.png" style="border: 0pt none ;" alt="profile-general"/></a>Les options de cet onglet contient des options relatives au conteneur et à la méthode d'endage.</p>
    <h4>Conteneur</h4>
    <ul>
      <li><i>Format:</i> Le conteneur à utiliser pour intégrer les flux vidéo, audio et les sous-titres. Il contient également toutes les informations de synchronisation nécessaires à une bonne lecture du film.</li>
      <li><i>FourCC:</i> Le FourCC est un code à quatre caractères qui identifie de façon unique le format d'un flux vidéo. Il n'est généralement pas nécessaire de forcer le FourCC, mais certaines platines de salon reconnaissent seulement certains FourCC.</li>
      <li><i>Assure la synchronisation A/V:</i> Cette option permet d'améliorer la synchronisation entre les flux vidéo et audio en encodant en même temps que la vidéo une piste audio en très basse qualité. En contrepartie, l'encodage prendra un peu plus de temps et d'espace disque. Au final, cette option est peu coûteuse et semble apporter un réel gain.</li>
    </ul>
    <h4>Encodage</h4>
    <p>OGMRip propose trois méthodes d'encodage:</p>
    <ul>
      <li><i>Taille fixe:</i> OGMRip peut estimer le taux d'échantillonnage à utiliser pour encoder la vidéo. Pour ce faire, il se base sur la taille totale du fichier qu'il doit produire. Ce fichier pourra ensuite être divisé en plusieurs morceaux. Les options de taille permettent donc de spécifier le nombre de cible à générer et la taille de chaque cible.</li>
      <li><i>Bitrate constant:</i> L'utilisateur founit le taux d'échantillonnage (en kilo bits par seconde) pour encoder la vidéo. Plus le taux est élevé, meilleure sera la qualité.</li>
      <li><i>Quantum constant:</i> Appelée également qualité constante, cette option permet d'obtenir des encodages de très grande qualité au détriment de la taille du fichier obtenu. Plus le quantum est bas, meilleure sera la qualité.</li>
    </ul>
    <h3 id="profil_video">Vidéo</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-profile-video.png"><img src="shots/ogmrip-profile-video-small.png" style="border: 0pt none ;" alt="profile-video"/></a>Les options de cet onglet concernent uniquement l'encodage du flux vidéo.</p>
    <h4>Codec</h4>
    <p>Le codec pour effectuer la compression vidéo. Quatre codecs vidéo sont actuellement supportés par OGMRip mais seuls ceux pour lesquels il a été configuré sont affichés ici. XviD et Lavc Mpeg-4 sont des implémentations de la norme Mpeg-4 (partie 2) de codage vidéo; Ogg Theora est basé sur le codec libéré VP3 de On2 Technologies; X264 permet de coder des flux vidéo au format H.264/AVC.</p>
    <h4>Options</h4>
    <ul>
      <li><i>Passes:</i> Spécifie le nombre de passes à effectuer pour encoder le flux vidéo. Il est recommandé de toujours encoder en mode 2 ou 3 passes puisque cela permet une distribution plus adéquate des bits et améliore la qualité globale.</li>
      <li><i>Qualité:</i> Cette option définit la qualité de l'encodage. Attention tout de même, la qualité la plus haute peut être extrêmement lente.</li>
      <li><i>Réduction du bruit de l'image:</i> Cette option permet de réduire le bruit de l'image pour que les plans fixes soient vraiment fixes ce qui peut améliorer la compressibilité.</li>
      <li><i>Recherche de quantification par treillis:</i> La quantification treillis est un type d'encodage adaptatif qui permet d'économiser des bits en modifiant les coefficients de quantification pour augmenter la compressibilité de la vidéo.</li>
      <li><i>Utilisation de la compensation de mouvement par quart de pixel:</i> MPEG-4 recherche par défaut les mouvements avec une précision d'un demi-pixel. Il est néanmoins possible de faire une recherche avec une précision d'un quart de pixel, ce qui permet généralement d'obtenir une image plus détaillée et d'économiser des bits en description de mouvement. Malheureusement, cette plus grande précision consomme une partie de la bande passante vidéo, ainsi cette option peut aussi bien dégrader la qualité de l'image que l'améliorer.</li>
      <li><i>Utilisation d'un algorithme de déblocage:</i> TODO.</li>
      <li><i>Utilisation d'un algorithme de deringing:</i> TODO.</li>
      <li><i>Turbo:</i> Accélère énormément la première passe en utilisant des algorithmes plus rapides et en désactivant des options gourmandes en temps processeur.</li>
    </ul>
    <h4>Plus d'options</h4>
    <ul>
      <li><i>Découpage de l'image:</i> Quand cette option est cochée, l'image peut être découpée.</li>
      <li><i>Redimensionnement:</i> Pour optimiser la qualité de l'encodage, il est systématiquement nécessaire de réduire la taille de l'image. Cette option détermine l'algorithme de redimensionnement à utiliser. L'option par défaut est recommandée par MPlayer.</li>
      <li><i>Taille minimum de l'image:</i> Cette options permet de spécifier la taille minimum en-dessous de laquelle l'image ne sera pas redimensionnée. Pour ne pas définir de taille minimum, il suffit de mettre la largeur ou la hauteur à 0.</li>
      <li><i>Taille maximum de l'image:</i> Cette options permet de spécifier la taille maximum au-delà de laquelle l'image ne sera pas redimensionnée. Pour ne pas définir de taille maximum, il suffit de mettre la largeur ou la hauteur à 0.</li>
      <li><i>Étendre l'image à la taille maximum:</i> Avec cette option, l'image sera étendue à la taille maximum en ajoutant des barres noires autour d'elle.</li>
    </ul>
    <h3 id="profil_audio">Audio</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-profile-audio.png"><img src="shots/ogmrip-profile-audio-small.png" style="border: 0pt none ;" alt="profile-audio"/></a>Les options de cet onglet concernent uniquement l'encodage des pistes audio.</p>
    <h4>Codecs</h4>
    <p>Le codec pour effectuer la compression audio. Quatre codecs audio sont actuellement supportés par OGMRip mais seuls ceux pour lesquels il a été configuré sont affichés ici. OGMRip peut aussi directement extraire les pistes audio sans transcodage et ainsi conserver la qualité des formats AC3 ou DTS contre une plus grande quantité de données. Le MP3 est un format de compression avec perte de qualité sonore significative mais acceptable pour l'oreille humaine. Ogg Vorbis est plus performant en terme de qualité et taux de compression mais peu supporté par les platines DivX de salon. WAV est le format le plus courant pour l'audio non compressé. L'Advanced Audio Coding ou AAC offre un meilleur ratio qualité/compression que le MP3.</p>
    <h4>Options</h4>
    <ul>
      <li><i>Qualité:</i> Cette option spécifie la qualité de l'encodage des pistes audio, 0 étant la plus basse qualité, 10, la plus haute.</li>
      <li><i>Canaux:</i> Permet de réduire le nombre de canaux en fusionnant leur contenu.</li>
      <li><i>Taux d'échantillonnage:</i> Permet de réduire le taux d'échantillonnage des pistes audio.</li>
      <li><i>Normalisation:</i> Augmente le volume sans introduire de distorsions sonores.</li>
    </ul>
    <h3 id="profil_subp">Sous-titres</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-profile-subp.png"><img src="shots/ogmrip-profile-subp-small.png" style="border: 0pt none ;" alt="profile-subp"/></a>Les options de cet onglet concernent uniquement l'extraction des sous-titres.</p>
    <h4>Codecs</h4>
    <p>Le format des sous-titres. Deux formats sont actuellement supportés par OGMRip mais seuls ceux pour lesquels il a été configuré sont affichés ici. SRT ou SubRip est un format texte extrêmement simple mais suffisant dans la plupart des cas. VobSub est le format des sous-titres sur DVD. Ce n'est pas un format texte, chaque sous-titre est stocké sous forme d'image.</p>
    <ul>
      <li><i>Sous-titres forcés seulement:</i> Extrait seulement les sous-titres forcés.</li>
    </ul>
    <h4>Options texte</h4>
    <p>(Ces options sont accessibles seulement si le codec sélectionné permet d'extraire des sous-titres au format texte).</p>
    <ul>
      <li><i>Jeu de caractères:</i> Le jeu de caractères à utiliser pour les sous-titres. Les platines de salon semblent préférer l'ISO-8859 ou l'ASCII mais certains caractères pourront uniquement être représentés en UTF-8.</li>
      <li><i>Fin de ligne:</i> Les caractères de fin de ligne diffèrent sous DOS/Windows et sous UNIX. Certaines platines de salon préfèrent le format DOS.</li>
      <li><i>Vérification orthographique:</i> La reconnaissance optique des caractères n'est pas fiable à 100% et il est souvent nécessaire d'effectuer une correction orthographique.</li>
    </ul>
    <h2 id="encodage">Encodage</h2>
    <p>L'encodage s'effectue en plusieurs temps. Le premier consiste à sélectionner la source qui peut se présenter sous la forme d'un DVD, d'une copie d'un DVD sur disque dur ou d'une image ISO. Ensuite, il faut choisir le titre, les pistes audio, les sous-titres et les chapitres à encoder. Enfin, il faut déterminer certaines options spécifiques à chaque encodage.</p>
    <h3 id="selection_source">Sélection de la source</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-load.png"><img src="shots/ogmrip-load-small.png" style="border: 0pt none ;" alt="load"/></a>Pour sélectionner la source, cliquez sur "Charger" pour afficher la fenêtre de sélection des DVD. Vous pouvez alors choisir dans la liste un DVD vidéo ou parcourir le disque pour rechercher un répertoire ou une image ISO. Si vous souhaitez encoder un disque qui n'est pas dans le lecteur, vous pouvez cliquer sur "Ejecter" pour en changer. Une fois votre choix effectué, cliquer sur "Charger" et le contenu du DVD sera affiché.</p>
    <p>Il est également possible de specifier un périphérique, un répertoire ou une image ISO directement sur la ligne de commande:</p>
    <p>
      <tt>$ ogmrip image.iso</tt>
    </p>
    <h3 id="fenetre_principale">Fenêtre principale</h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-main.png"><img src="shots/ogmrip-main-small.png" style="border: 0pt none ;" alt="main"/></a>Une fois le DVD chargé, la fenêtre principale présente la table des matières du DVD triée par titre. Chaque fois qu'un nouveau titre est sélectionné, les pistes audio, les sous-titres, les informations de chapitrage et la durée changent en fonction.</p>
    <h4>Titre et chapitrage</h4>
    <p>Le titre du DVD est automatiquement extrait. Il servira de base pour tous les noms de fichier. Le nom des chapitres, par contre, n'est pas contenu dans le DVD. Il est donc nécessaire de les saisir à la main si besoin. Pour ce faire, il suffit de double cliquer dans la colonne Nom de la ligne concernée dans la liste des chapitres. Les chapitres peuvent également être importer depuis un fichier au format simple ou au format XML Matroska (voir mkvmerge) grâce au menu "Importer chapitrage". Seuls les containers OGM et Matroska permettent de stocker les informations de chapitrage. Elles pourront être utilisées par les lecteurs multimédia pour effectuer la navigation par chapitre.</p>
    <p>Les information de chapitrage peuvent également être exportées pour usage ultérieur grâce au menu "Exporter chapitrage".</p>
    <h4>Choix du titre vidéo, des pistes audio et des sous-titres</h4>
    <p>OGMRip ne peut encoder qu'un seul titre vidéo à la fois; par défaut, ce sera le titre le plus long. OGMRip permet, par contre, d'encoder plusieurs pistes audio et plusieurs sous-titres pour un même titre vidéo si le container le permet. C'est le cas de Matroska et d'OGM. Ce devrait également l'être pour AVI et MP4, mais MEncoder ne le permet pas. Par défaut, c'est la première piste audio et les premiers sous-titres dans les langues préférées qui sont sélectionnés. Pour en ajouter de nouveau, il suffit de cliquer sur les boutons '+' et de sélectionner le flux approprié. Les boutons '-' permettent de supprimer les flux associés.</p>
    <p>OGMRip supporte également les flux audio et les fichiers de sous-titres externes.</p>
    <h4>Sélection de chapitres et mode relatif</h4>
    <p>Par défaut, tous les chapitres d'un titre vidéo sont sélectionnés pour extraction. Il est pourtant possible de n'extraire qu'un sous-ensemble contigu de chapitres. Il suffit pour cela de cliquer sur les boîtes à cocher à côté de chaque chapitre pour les sélectionner ou les désélectionner. Dès que quelques chapitres seulement sont choisis, la boîte à cocher 'Mode relatif' devient accessible. Dans ce mode, OGMRip déterminera le taux d'échantillonnage comme si la totalité du titre était extraite. De ce fait, ce mode permet d'obtenir des aperçus de la qualité en ne sélectionnant qu'un chapitre ou deux.</p>
    <h3 id="options">Options </h3>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-options.png"><img src="shots/ogmrip-options-small.png" style="border: 0pt none ;" alt="options"/></a>Certaines options ne peuvent pas être spécifiées globalement, elles sont spécifiques à chaque encodage.</p>
    <ul>
      <li><i>Profil:</i> Cette liste déroulante permet de choisir le profil d'encodage, ce qui permet de déterminer le conteneur, les codecs et surtout leurs options.</li>
      <li><i>Découpage automatique:</i> Par défaut, OGMRip détermine automatiquement les paramètres de découpage des bandes noires au-dessus et au-dessous de l'image. Dans certains cas, cette détection peut être erronée et il est nécessaire de stipuler des paramètres personnalisés.</li>
      <li><i>Redimensionnement automatique:</i> Là encore, OGMRip tente d'optimiser l'encodage en redimensionnant la vidéo afin de respecter un nombre de bits par pixel supposé optimal.  Il est pourtant possible d'indiquer une taille d'image personnalisée et OGMRip propose un certain nombre de réglages prédéfinis qui respectent le rapport hauteur largeur de l'image. Il faut toutefois noter que les paramètres de redimensionnement ne pourront être autodétectés que si un taux d'échantillonnage et des paramètres de découpage ont été spécifiés explicitement.</li>
      <li><i>Test de compressibilité automatique::</i> Il est également possible d'améliorer la quality d'un encodage en testant la compressibilité du film puis en ajustant les paramètres de redimensionnement en conséquence. Par défaut, OGMRip effectue automatiquement un test de compressibilité, mais il est possible de désactiver ce comportement.</li>
      <li><i>Dessin animé:</i> Cette option active certaines optimisation des codecs vidéo pour améliorer l'encodage des dessins animés.</li>
      <li><i>Désentrelacement:</i> Pour rendre la vidéo la plus compatible possible, il peut être nécessaire de la désentrelacer. En revanche, pour conserver une qualité maximum, autant encoder la vidéo entrelacée et la désentrelacer au besoin lors de la diffusion.</li>
    </ul>
    <p>Vous pouvez maintenant cliquer sur le bouton "Extraire" pour démarrer immédiatement l'encodage du film, ou alors cliquer sur le bouton "Enqueue" et l'ajouter au gestionnaire d'encodages.</p>
    <h2 id="gestionnaire">Gestionnaire d'encodages</h2>
    <p><a style="border: 0pt none ; background-color: transparent; clear: right; margin-bottom: 1em; float: right; margin-left: 1em;" href="shots/ogmrip-encoding-queue.png"><img src="shots/ogmrip-encoding-queue-small.png" style="border: 0pt none ;" alt="encoding-queue"/></a>Encoder de multiples DVD peut rapidement devenir long et fastidieux puisqu'il faut attendre la fin d'un encodage avec d'en commencer un nouveau. C'est pourquoi OGMRip dispose d'un gestionnaire d'encodages.</p>
    <p>Il fonctionne comme suit:</p>
    <ul>
      <li>si nécessaire, il copie tous les titres des DVD sur le disque dur</li>
      <li>il encode ensuite les titres des DVD</li>
      <li>dès qu'un titre n'est plus nécessaire, il est supprimé du disque dur</li>
    </ul>
    <p>De cette façon, vous n'avez qu'à attendre que les titres des DVD soient copies, les encodages ne nécessitant plus aucune intervention humaine.</p>
    <h2 id="conclusion">Conclusion</h2>
    <p>Encoder une vidéo est un processus complexe; obtenir une vidéo de qualité est d'autant plus difficile.  OGMRip tente au mieux d'automatiser l'enchaînement des traitements et d'optimiser les options et s'en sort plutôt bien. Il reste cependant certains cas où OGMRip ne propose pas suffisamment de finesse dans les réglages et certains encodages n'auront pas une synchronisation audio/video optimale. Il ne reste plus que l'encodage manuel pour essayer de corriger les problèmes résiduels.</p>
    <h2 id="annexes">Annexes</h2>
    <p>OGMRip fournit également un certain nombre d'exécutables qui peuvent être utilisés indépendamment.</p>
    <h3 id="dvdcpy">dvdcpy</h3>
    <p>dvdcpy permet de copier sur disque dur un DVD complet, seulement quelques titres, avec ou sans le menu principal.</p>
    <pre>$ dvdcpy -h
Usage:
  dvdcpy [OPTION...] &lt;DVD DEVICE&gt;

Help Options:
  -h, --help              Show help options

Application Options:
  -o, --output output     Specify the output directory
  -t, --title title       Select the title to copy. This option may be
                          specified multiple times (default: all titles)
  -m, --menu              Copy the DVD menu</pre>
    <h3 id="subptools">subptools</h3>
    <p>subptools est le successeur de srttool (voir <a href="http://subtitleripper.sourceforge.net/" title="subtitleripper">subtitleripper</a>) et de srtutil. Il permet de manipuler des fichiers de sous-titres au format XML fournis par subp2pgm, subp2png et subp2tiff et de les convertir au format SRT ou spumux.</p>
    <pre>$ subptools -h
Usage:
  subptools [OPTION...]

Help Options:
  -h, --help                     Show help options

Application Options:
  -v, --verbose                  Verbose
  -r, --renumber                 Renumber all entries
  -s, --subst                    Substitute filename after time stamps by its
                                 content
  -w, --strip                    Remove leading white space in text lines
  -d, --shift=&lt;time&gt;             Shift all timestamps by &lt;time&gt; seconds
  -c, --cut=&lt;first[,last]&gt;       Write only entries from first to last (default:
                                 all entries) to output
  -a, --adjust=&lt;hh:mm:ss,ms&gt;     Adjust all time stamps so that the first tag
                                 begins at hh:mm:ss,ms
  -e, --expand=&lt;seconds&gt;         Expand the subtitle hour by &lt;seconds&gt;
  -t, --convert=&lt;format&gt;         Convert the subtitle in the given format (srt,
                                 spumux)
  -i, --input=&lt;filename&gt;         Use filename for input (default: stdin)
  -o, --output=&lt;filename&gt;        Use filename for output (default: stdout)</pre>
    <h3 id="subp2pgm">subp2pgm, subp2png, subp2tiff</h3>
    <p>subp2pgm, subp2png et subp2tiff permettent d'extraire des images au format PGM, PNG et TIFF respectivement à partir de sous-titres au format VobSub.</p>
    <pre>$ subp2pgm -h
Usage:
  subp2pgm [OPTION...] &lt;vobsub basename&gt;

Help Options:
  -h, --help                  Show help options

Application Options:
  -o, --output=&lt;filename&gt;     Use filename for output (default: stdout)
  -s, --sid=&lt;sid&gt;             The subtitle id (default: 0)
  -f, --forced                Extract only forced subtitles
  -v, --verbose               Increase verbosity level</pre>
    <h3 id="theoraenc">theoraenc</h3>
    <p>theoraenc est un encodeur Ogg Theora extrêmement simple et minimaliste.</p>
    <pre>$ theoraenc -h
Usage:
  theoraenc [OPTION...] &lt;VIDEO FILE>

Help Options:
  -h, --help        Show help options

Application Options:
  -o, --output=&lt;filename&gt;     Use filename for output (default: stdout)
  -b, --bitrate=&lt;bitrate&gt;     Bitrate target for Theora video
  -q, --quality=&lt;quality&gt;     Theora quality selector from 0 to 10 (0 yields
                              smallest files but lowest video quality. 10 yields
                              highest fidelity but large files)
  -a, --aspect=&lt;aspect&gt;       Set the aspect ratio of the input file
  -f, --framerate=&lt;rate&gt;      Set the framerate of the input file

theoraenc only accepts YUV4MPEG2 uncompressed video.</pre>
    <p/>
  </content>
  <sidebar>
    <p>
      <a href="#introduction">Introduction</a><br/>
      <a href="#installation">Installation</a><br/>
      &#xA0;&#xA0;<a href="#composants_requis">Composants requis</a><br/>
      &#xA0;&#xA0;<a href="#composants_optionnels">Composants optionnels</a><br/>
      &#xA0;&#xA0;<a href="#options_conf">Options de configuration</a><br/>
      <a href="#preferences">Préférences</a><br/>
      &#xA0;&#xA0;<a href="#pref_generales">Générales</a><br/>
      &#xA0;&#xA0;<a href="#pref_avancees">Avancées</a><br/>
      <a href="#profils">Profils</a><br/>
      &#xA0;&#xA0;<a href="#profil_general">Général</a><br/>
      &#xA0;&#xA0;<a href="#profil_video">Vidéo</a><br/>
      &#xA0;&#xA0;<a href="#profil_audio">Audio</a><br/>
      &#xA0;&#xA0;<a href="#profil_subp">Sous-titres</a><br/>
      <a href="#encodage">Encodage</a><br/>
      &#xA0;&#xA0;<a href="#selection_source">Sélection de la source</a><br/>
      &#xA0;&#xA0;<a href="#fenetre_principale">Fenêtre principale</a><br/>
      &#xA0;&#xA0;<a href="#options">Options</a><br/>
      <a href="#gestionnaire">Gestionnaire d'encodages</a><br/>
      <a href="#conclusion">Conclusion</a><br/>
      <a href="#annexes">Annexes</a><br/>
      &#xA0;&#xA0;<a href="#dvdcpy">dvdcpy</a><br/>
      &#xA0;&#xA0;<a href="#subptools">subptools</a><br/>
      &#xA0;&#xA0;<a href="#subp2pgm">subp2pgm</a><br/>
      &#xA0;&#xA0;<a href="#theoraenc">theoraenc</a><br/>
    </p>
  </sidebar>
</html>
