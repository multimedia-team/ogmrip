<SECTION>
<FILE>ogmjob-list</FILE>
<TITLE>OGMJobList</TITLE>
OGMJobList
ogmjob_list_get_children
<SUBSECTION Standard>
OGMJOB_LIST
OGMJOB_IS_LIST
OGMJOB_TYPE_LIST
ogmjob_list_get_type
OGMJOB_LIST_CLASS
OGMJOB_IS_LIST_CLASS
OGMJOB_LIST_GET_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-queue</FILE>
<TITLE>OGMJobQueue</TITLE>
OGMJobQueue
ogmjob_queue_new
<SUBSECTION Standard>
OGMJOB_QUEUE
OGMJOB_IS_QUEUE
OGMJOB_TYPE_QUEUE
ogmjob_queue_get_type
OGMJOB_QUEUE_CLASS
OGMJOB_IS_QUEUE_CLASS
OGMJOB_QUEUE_GET_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-spawn</FILE>
<TITLE>OGMJobSpawn</TITLE>
OGMJobSpawn
ogmjob_spawn_run
ogmjob_spawn_cancel
ogmjob_spawn_suspend
ogmjob_spawn_resume
ogmjob_spawn_set_async
ogmjob_spawn_get_async
ogmjob_spawn_get_parent
ogmjob_spawn_set_parent
ogmjob_spawn_propagate_error
<SUBSECTION Standard>
OGMJOB_SPAWN
OGMJOB_IS_SPAWN
OGMJOB_TYPE_SPAWN
ogmjob_spawn_get_type
OGMJOB_SPAWN_CLASS
OGMJOB_IS_SPAWN_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-bin</FILE>
<TITLE>OGMJobBin</TITLE>
OGMJobBin
ogmjob_bin_get_child
<SUBSECTION Standard>
OGMJOB_BIN
OGMJOB_IS_BIN
OGMJOB_TYPE_BIN
ogmjob_bin_get_type
OGMJOB_BIN_CLASS
OGMJOB_IS_BIN_CLASS
OGMJOB_BIN_GET_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-pipeline</FILE>
<TITLE>OGMJobPipeline</TITLE>
OGMJobPipeline
ogmjob_pipeline_new
<SUBSECTION Standard>
OGMJOB_PIPELINE
OGMJOB_IS_PIPELINE
OGMJOB_TYPE_PIPELINE
ogmjob_pipeline_get_type
OGMJOB_PIPELINE_CLASS
OGMJOB_IS_PIPELINE_CLASS
OGMJOB_PIPELINE_GET_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-container</FILE>
OGMJobCallback
<TITLE>OGMJobContainer</TITLE>
OGMJobContainer
ogmjob_container_add
ogmjob_container_remove
ogmjob_container_foreach
<SUBSECTION Standard>
OGMJOB_CONTAINER
OGMJOB_IS_CONTAINER
OGMJOB_TYPE_CONTAINER
ogmjob_container_get_type
OGMJOB_CONTAINER_CLASS
OGMJOB_IS_CONTAINER_CLASS
OGMJOB_CONTAINER_GET_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-exec</FILE>
OGMJobWatch
<TITLE>OGMJobExec</TITLE>
OGMJobExec
ogmjob_exec_new
ogmjob_exec_newv
ogmjob_exec_get_status
ogmjob_exec_add_watch
ogmjob_exec_add_watch_full
<SUBSECTION Standard>
OGMJOB_EXEC
OGMJOB_IS_EXEC
OGMJOB_TYPE_EXEC
ogmjob_exec_get_type
OGMJOB_EXEC_CLASS
OGMJOB_IS_EXEC_CLASS
</SECTION>

<SECTION>
<FILE>ogmjob-log</FILE>
ogmjob_log_open
ogmjob_log_close
ogmjob_log_write
ogmjob_log_printf
ogmjob_log_set_print_stdout
ogmjob_log_get_print_stdout
ogmjob_log_set_print_stderr
ogmjob_log_get_print_stderr
</SECTION>

