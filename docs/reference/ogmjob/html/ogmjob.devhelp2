<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE book PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "">
<book xmlns="http://www.devhelp.net/book" title="OGMJob Reference Manual" link="index.html" author="" name="ogmjob" version="2" language="c">
  <chapters>
    <sub name="Types and functions" link="ogmjob-ref.html">
      <sub name="Logs" link="ogmjob-Logs.html"/>
    </sub>
    <sub name="OGMJob Library Reference" link="ogmjob-api.html">
      <sub name="Spawn Classes" link="spawn-classes.html">
        <sub name="OGMJobExec" link="OGMJobExec.html"/>
        <sub name="OGMJobQueue" link="OGMJobQueue.html"/>
        <sub name="OGMJobPipeline" link="OGMJobPipeline.html"/>
      </sub>
      <sub name="Abstract Base Classes" link="abstract-classes.html">
        <sub name="OGMJobSpawn" link="OGMJobSpawn.html"/>
        <sub name="OGMJobContainer" link="OGMJobContainer.html"/>
        <sub name="OGMJobBin" link="OGMJobBin.html"/>
        <sub name="OGMJobList" link="OGMJobList.html"/>
      </sub>
    </sub>
  </chapters>
  <functions>
    <keyword type="function" name="ogmjob_log_open ()" link="ogmjob-Logs.html#ogmjob-log-open"/>
    <keyword type="function" name="ogmjob_log_close ()" link="ogmjob-Logs.html#ogmjob-log-close"/>
    <keyword type="function" name="ogmjob_log_write ()" link="ogmjob-Logs.html#ogmjob-log-write"/>
    <keyword type="function" name="ogmjob_log_printf ()" link="ogmjob-Logs.html#ogmjob-log-printf"/>
    <keyword type="function" name="ogmjob_log_set_print_stdout ()" link="ogmjob-Logs.html#ogmjob-log-set-print-stdout"/>
    <keyword type="function" name="ogmjob_log_get_print_stdout ()" link="ogmjob-Logs.html#ogmjob-log-get-print-stdout"/>
    <keyword type="function" name="ogmjob_log_set_print_stderr ()" link="ogmjob-Logs.html#ogmjob-log-set-print-stderr"/>
    <keyword type="function" name="ogmjob_log_get_print_stderr ()" link="ogmjob-Logs.html#ogmjob-log-get-print-stderr"/>
    <keyword type="function" name="OGMJobWatch ()" link="OGMJobExec.html#OGMJobWatch"/>
    <keyword type="function" name="ogmjob_exec_new ()" link="OGMJobExec.html#ogmjob-exec-new"/>
    <keyword type="function" name="ogmjob_exec_newv ()" link="OGMJobExec.html#ogmjob-exec-newv"/>
    <keyword type="function" name="ogmjob_exec_get_status ()" link="OGMJobExec.html#ogmjob-exec-get-status"/>
    <keyword type="function" name="ogmjob_exec_add_watch ()" link="OGMJobExec.html#ogmjob-exec-add-watch"/>
    <keyword type="function" name="ogmjob_exec_add_watch_full ()" link="OGMJobExec.html#ogmjob-exec-add-watch-full"/>
    <keyword type="struct" name="struct OGMJobExec" link="OGMJobExec.html#OGMJobExec-struct"/>
    <keyword type="function" name="ogmjob_queue_new ()" link="OGMJobQueue.html#ogmjob-queue-new"/>
    <keyword type="struct" name="struct OGMJobQueue" link="OGMJobQueue.html#OGMJobQueue-struct"/>
    <keyword type="function" name="ogmjob_pipeline_new ()" link="OGMJobPipeline.html#ogmjob-pipeline-new"/>
    <keyword type="struct" name="struct OGMJobPipeline" link="OGMJobPipeline.html#OGMJobPipeline-struct"/>
    <keyword type="function" name="ogmjob_spawn_run ()" link="OGMJobSpawn.html#ogmjob-spawn-run"/>
    <keyword type="function" name="ogmjob_spawn_cancel ()" link="OGMJobSpawn.html#ogmjob-spawn-cancel"/>
    <keyword type="function" name="ogmjob_spawn_suspend ()" link="OGMJobSpawn.html#ogmjob-spawn-suspend"/>
    <keyword type="function" name="ogmjob_spawn_resume ()" link="OGMJobSpawn.html#ogmjob-spawn-resume"/>
    <keyword type="function" name="ogmjob_spawn_set_async ()" link="OGMJobSpawn.html#ogmjob-spawn-set-async"/>
    <keyword type="function" name="ogmjob_spawn_get_async ()" link="OGMJobSpawn.html#ogmjob-spawn-get-async"/>
    <keyword type="function" name="ogmjob_spawn_get_parent ()" link="OGMJobSpawn.html#ogmjob-spawn-get-parent"/>
    <keyword type="function" name="ogmjob_spawn_set_parent ()" link="OGMJobSpawn.html#ogmjob-spawn-set-parent"/>
    <keyword type="function" name="ogmjob_spawn_propagate_error ()" link="OGMJobSpawn.html#ogmjob-spawn-propagate-error"/>
    <keyword type="struct" name="struct OGMJobSpawn" link="OGMJobSpawn.html#OGMJobSpawn-struct"/>
    <keyword type="signal" name="The “cancel” signal" link="OGMJobSpawn.html#OGMJobSpawn-cancel"/>
    <keyword type="signal" name="The “progress” signal" link="OGMJobSpawn.html#OGMJobSpawn-progress"/>
    <keyword type="signal" name="The “resume” signal" link="OGMJobSpawn.html#OGMJobSpawn-resume"/>
    <keyword type="signal" name="The “run” signal" link="OGMJobSpawn.html#OGMJobSpawn-run"/>
    <keyword type="signal" name="The “suspend” signal" link="OGMJobSpawn.html#OGMJobSpawn-suspend"/>
    <keyword type="function" name="OGMJobCallback ()" link="OGMJobContainer.html#OGMJobCallback"/>
    <keyword type="function" name="ogmjob_container_add ()" link="OGMJobContainer.html#ogmjob-container-add"/>
    <keyword type="function" name="ogmjob_container_remove ()" link="OGMJobContainer.html#ogmjob-container-remove"/>
    <keyword type="function" name="ogmjob_container_foreach ()" link="OGMJobContainer.html#ogmjob-container-foreach"/>
    <keyword type="struct" name="struct OGMJobContainer" link="OGMJobContainer.html#OGMJobContainer-struct"/>
    <keyword type="signal" name="The “add” signal" link="OGMJobContainer.html#OGMJobContainer-add"/>
    <keyword type="signal" name="The “remove” signal" link="OGMJobContainer.html#OGMJobContainer-remove"/>
    <keyword type="function" name="ogmjob_bin_get_child ()" link="OGMJobBin.html#ogmjob-bin-get-child"/>
    <keyword type="struct" name="struct OGMJobBin" link="OGMJobBin.html#OGMJobBin-struct"/>
    <keyword type="function" name="ogmjob_list_get_children ()" link="OGMJobList.html#ogmjob-list-get-children"/>
    <keyword type="struct" name="struct OGMJobList" link="OGMJobList.html#OGMJobList-struct"/>
  </functions>
</book>
