
#ifndef __ogmdvd_cclosure_marshal_MARSHAL_H__
#define __ogmdvd_cclosure_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:STRING,UINT (ogmdvd-marshal.list:24) */
extern void ogmdvd_cclosure_marshal_VOID__STRING_UINT (GClosure     *closure,
                                                       GValue       *return_value,
                                                       guint         n_param_values,
                                                       const GValue *param_values,
                                                       gpointer      invocation_hint,
                                                       gpointer      marshal_data);

G_END_DECLS

#endif /* __ogmdvd_cclosure_marshal_MARSHAL_H__ */

