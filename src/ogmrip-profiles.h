/* OGMRip - A DVD Encoder for GNOME
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __OGMRIP_PROFILES_H__
#define __OGMRIP_PROFILES_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

gboolean    ogmrip_profiles_reload            (const gchar          *dirname,
                                               const gchar          *profile,
                                               GError               **error);
gboolean    ogmrip_profiles_import            (const gchar          *filename,
                                               GError               **error);
gboolean    ogmrip_profiles_import_all        (const gchar          *dirname,
                                               GError               **error);
gboolean    ogmrip_profiles_check_profile     (const gchar          *profile,
                                               GError               **error);
GList *     ogmrip_profiles_check_updates     (GList                *list,
                                               const gchar          *dirname,
                                               GError               **error);

G_END_DECLS

#endif /* __OGMRIP_PROFILES_H__ */

