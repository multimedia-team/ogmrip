/* OGMRip - A DVD Encoder for GNOME
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ogmdvd.h"
#include "ogmdvd-gtk.h"
#include "ogmrip.h"

#include "ogmrip-gconf.h"
#include "ogmrip-helper.h"

#include "ogmrip-pref-dialog.h"
#include "ogmrip-profiles-dialog.h"

#include <locale.h>
#include <string.h>
#include <glib/gi18n.h>
#include <glade/glade.h>

#define OGMRIP_GLADE_FILE "ogmrip" G_DIR_SEPARATOR_S "ogmrip-pref.glade"
#define OGMRIP_GLADE_ROOT "root"

#define OGMRIP_PREF_DIALOG_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), OGMRIP_TYPE_PREF_DIALOG, OGMRipPrefDialogPriv))

struct _OGMRipPrefDialogPriv
{
  gboolean dummy;
};

extern OGMRipSettings *settings;

/*
 * Lang
 */

static void
ogmrip_pref_dialog_lang_get_value (GObject *combo, const gchar *property, GValue *value, gpointer data)
{
  GtkTreeIter iter;

  if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo), &iter))
  {
    GtkTreeModel *model;
    guint lang;

    model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));

    gtk_tree_model_get (model, &iter, 0, &lang, -1);
    g_value_set_int (value, lang);
  }
}

static void
ogmrip_pref_dialog_lang_set_value (GObject *combo, const gchar *property, const GValue *value, gpointer key)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gint code;

  code = g_value_get_int (value);
  if (!code && !g_str_equal (key, OGMRIP_GCONF_PREF_SUBP))
  {
    gchar *locale;

    locale = g_get_locale (LC_ALL);
    if (locale && strlen (locale) > 2)
    {
      code = locale[0];
      code = (code << 8) | locale[1];

      ogmrip_settings_set (settings, OGMRIP_GCONF_GENERAL, key, code, NULL);
    }
    g_free (locale);
  }

  if (!code)
    gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
  else
  {
    model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
    if (gtk_tree_model_get_iter_first (model, &iter))
    {
      guint lang;

      do
      {
        gtk_tree_model_get (model, &iter, 0, &lang, -1);
        if (lang == code)
        {
          gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo), &iter);
          break;
        }
      }
      while (gtk_tree_model_iter_next (model, &iter));

      if (lang != code)
        gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
    }
  }
}

/*
 * Chooser
 */

static void
ogmrip_pref_dialog_output_chooser_notified (OGMRipSettings *settings, const gchar *section, const gchar *key, const GValue *value, GtkWidget *chooser)
{
  const gchar *new_path;
  gchar *old_path;

  new_path = g_value_get_string (value);
  if (g_file_test (new_path, G_FILE_TEST_IS_DIR))
  {
    old_path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (chooser));
    if (!old_path || !g_str_equal (new_path, old_path))
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (chooser), new_path);
  }
}

static void
ogmrip_pref_dialog_output_chooser_changed (GtkWidget *chooser)
{
  gchar *str;

  str = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (chooser));
  if (str)
  {
    ogmrip_settings_set (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_OUTPUT_DIR, str, NULL);
    g_free (str);
  }
}

static void
ogmrip_pref_dialog_tmp_chooser_notified (OGMRipSettings *settings, const gchar *section, const gchar *key, const GValue *value, GtkWidget *chooser)
{
  const gchar *new_path;
  gchar *old_path;

  new_path = g_value_get_string (value);
  if (g_file_test (new_path, G_FILE_TEST_IS_DIR))
  {
    ogmrip_fs_set_tmp_dir (new_path);

    old_path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (chooser));
    if (!old_path || !g_str_equal (new_path, old_path))
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (chooser), new_path);
  }
}

static void
ogmrip_pref_dialog_tmp_chooser_changed (GtkWidget *chooser)
{
  gchar *str;

  str = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (chooser));
  if (str)
  {
    ogmrip_settings_set (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_TMP_DIR, str, NULL);
    g_free (str);
  }
}

/*
 * Dialog
 */

G_DEFINE_TYPE (OGMRipPrefDialog, ogmrip_pref_dialog, GTK_TYPE_DIALOG)

static void
ogmrip_pref_dialog_class_init (OGMRipPrefDialogClass *klass)
{
  g_type_class_add_private (klass, sizeof (OGMRipPrefDialogPriv));
}

static void
ogmrip_pref_dialog_init (OGMRipPrefDialog *dialog)
{
  GtkWidget *area, *widget;
  GladeXML *xml;
  gchar *path;

  dialog->priv = OGMRIP_PREF_DIALOG_GET_PRIVATE (dialog);

  xml = glade_xml_new (OGMRIP_DATA_DIR G_DIR_SEPARATOR_S OGMRIP_GLADE_FILE, OGMRIP_GLADE_ROOT, NULL);
  if (!xml)
  {
    g_warning ("Could not find " OGMRIP_GLADE_FILE);
    return;
  }

  gtk_dialog_add_buttons (GTK_DIALOG (dialog),
      GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
      NULL);
#if !GTK_CHECK_VERSION(2,22,0)
  gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
#endif
  gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
  gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
  gtk_window_set_title (GTK_WINDOW (dialog), _("Preferences"));
  gtk_window_set_icon_from_stock (GTK_WINDOW (dialog), GTK_STOCK_PREFERENCES);

  area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  widget = glade_xml_get_widget (xml, OGMRIP_GLADE_ROOT);
  gtk_container_add (GTK_CONTAINER (area), widget);
  gtk_widget_show (widget);

  /*
   * General
   */
  widget = glade_xml_get_widget (xml, "output-dir-chooser");

  ogmrip_settings_get (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_OUTPUT_DIR, &path, NULL);
  if (!g_file_test (path, G_FILE_TEST_IS_DIR))
  {
    g_free (path);
    path = g_strdup (g_get_home_dir ());
    ogmrip_settings_set (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_OUTPUT_DIR, path);
  }
  gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (widget), path);
  g_free (path);

  ogmrip_settings_add_notify_while_alive (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_OUTPUT_DIR,
      (OGMRipNotifyFunc) ogmrip_pref_dialog_output_chooser_notified, widget, G_OBJECT (widget));
  g_signal_connect (widget, "current-folder-changed",
      G_CALLBACK (ogmrip_pref_dialog_output_chooser_changed), NULL);

  gtk_file_chooser_set_action (GTK_FILE_CHOOSER (widget), 
      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);

  widget = glade_xml_get_widget (xml, "filename-combo");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_FILENAME, G_OBJECT (widget), "active");

  widget = glade_xml_get_widget (xml, "pref-audio-combo");
  ogmrip_combo_box_languages_construct (GTK_COMBO_BOX (widget), _("No preferred audio language"));
  ogmrip_settings_bind_custom (settings,
      OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_PREF_AUDIO, G_OBJECT (widget), "active",
      ogmrip_pref_dialog_lang_get_value, ogmrip_pref_dialog_lang_set_value, OGMRIP_GCONF_PREF_AUDIO);

  widget = glade_xml_get_widget (xml, "pref-subp-combo");
  ogmrip_combo_box_languages_construct (GTK_COMBO_BOX (widget), _("No preferred subtitle language"));
  ogmrip_settings_bind_custom (settings,
      OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_PREF_SUBP, G_OBJECT (widget), "active",
      ogmrip_pref_dialog_lang_get_value, ogmrip_pref_dialog_lang_set_value, OGMRIP_GCONF_PREF_SUBP);

  widget = glade_xml_get_widget (xml, "chapter-lang-combo");
  ogmrip_combo_box_languages_construct (GTK_COMBO_BOX (widget), _("No chapters language"));
  ogmrip_settings_bind_custom (settings,
      OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_CHAPTER_LANG, G_OBJECT (widget), "active",
      ogmrip_pref_dialog_lang_get_value, ogmrip_pref_dialog_lang_set_value, OGMRIP_GCONF_CHAPTER_LANG);

  /*
   * Advanced
   */

  widget = glade_xml_get_widget (xml, "copy-dvd-check");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_ADVANCED, OGMRIP_GCONF_COPY_DVD, G_OBJECT (widget), "active");

  widget = glade_xml_get_widget (xml, "after-enc-combo");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_ADVANCED, OGMRIP_GCONF_AFTER_ENC, G_OBJECT (widget), "active");

  widget = glade_xml_get_widget (xml, "threads-spin");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_ADVANCED, OGMRIP_GCONF_THREADS, G_OBJECT (widget), "value");

  widget = glade_xml_get_widget (xml, "keep-tmp-check");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_ADVANCED, OGMRIP_GCONF_KEEP_TMP, G_OBJECT (widget), "active");

  widget = glade_xml_get_widget (xml, "log-check");
  ogmrip_settings_bind (settings,
      OGMRIP_GCONF_ADVANCED, OGMRIP_GCONF_LOG_OUTPUT, G_OBJECT (widget), "active");

  widget = glade_xml_get_widget (xml, "tmp-dir-chooser");

  ogmrip_settings_get (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_TMP_DIR, &path, NULL);
  if (!g_file_test (path, G_FILE_TEST_IS_DIR))
  {
    g_free (path);
    path = g_strdup (g_get_home_dir ());
    ogmrip_settings_set (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_TMP_DIR, path);
  }
  gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (widget), path);
  ogmrip_fs_set_tmp_dir (path);
  g_free (path);

  ogmrip_settings_add_notify_while_alive (settings, OGMRIP_GCONF_GENERAL, OGMRIP_GCONF_TMP_DIR,
      (OGMRipNotifyFunc) ogmrip_pref_dialog_tmp_chooser_notified, widget, G_OBJECT (widget));
  g_signal_connect (widget, "current-folder-changed",
      G_CALLBACK (ogmrip_pref_dialog_tmp_chooser_changed), NULL);

  gtk_file_chooser_set_action (GTK_FILE_CHOOSER (widget), 
      GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);

  g_object_unref (xml);
}

GtkWidget *
ogmrip_pref_dialog_new (void)
{
  return g_object_new (OGMRIP_TYPE_PREF_DIALOG, NULL);
}

