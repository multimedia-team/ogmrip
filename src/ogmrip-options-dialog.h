/* OGMRip - A DVD Encoder for GNOME
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __OGMRIP_OPTIONS_DIALOG_H__
#define __OGMRIP_OPTIONS_DIALOG_H__

#include <gtk/gtk.h>

#include <ogmrip-encoding.h>
#include <ogmdvd.h>

G_BEGIN_DECLS

typedef enum
{
  OGMRIP_OPTIONS_DIALOG_CREATE,
  OGMRIP_OPTIONS_DIALOG_EDIT
} OGMRipOptionsDialogAction;

enum
{
  OGMRIP_RESPONSE_EXTRACT,
  OGMRIP_RESPONSE_ENQUEUE,
  OGMRIP_RESPONSE_TEST
};

#define OGMRIP_TYPE_OPTIONS_DIALOG            (ogmrip_options_dialog_get_type ())
#define OGMRIP_OPTIONS_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), OGMRIP_TYPE_OPTIONS_DIALOG, OGMRipOptionsDialog))
#define OGMRIP_OPTIONS_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OGMRIP_TYPE_OPTIONS_DIALOG, OGMRipOptionsDialogClass))
#define OGMRIP_IS_OPTIONS_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, OGMRIP_TYPE_OPTIONS_DIALOG))
#define OGMRIP_IS_OPTIONS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OGMRIP_TYPE_OPTIONS_DIALOG))

typedef struct _OGMRipOptionsDialog      OGMRipOptionsDialog;
typedef struct _OGMRipOptionsDialogClass OGMRipOptionsDialogClass;
typedef struct _OGMRipOptionsDialogPriv  OGMRipOptionsDialogPriv;

struct _OGMRipOptionsDialog
{
  GtkDialog parent_instance;

  OGMRipOptionsDialogPriv *priv;
};

struct _OGMRipOptionsDialogClass
{
  GtkDialogClass parent_class;

  void (* profile_changed) (OGMRipOptionsDialog *dialog);
  void (* edit_clicked)    (OGMRipOptionsDialog *dialog);
};

GType            ogmrip_options_dialog_get_type               (void);

GtkWidget *      ogmrip_options_dialog_new                    (OGMRipOptionsDialogAction action);

void             ogmrip_options_dialog_set_response_sensitive (OGMRipOptionsDialog       *dialog,
                                                               guint                     type,
                                                               gboolean                  sensitive);
OGMRipEncoding * ogmrip_options_dialog_get_encoding           (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_set_encoding           (OGMRipOptionsDialog       *dialog,
                                                               OGMRipEncoding            *encoding);

gint             ogmrip_options_dialog_get_scale              (OGMRipOptionsDialog       *dialog, 
                                                               guint                     *width, 
                                                               guint                     *height);
void             ogmrip_options_dialog_set_scale              (OGMRipOptionsDialog       *dialog,
                                                               OGMRipOptionsType         type,
                                                               guint                     width,
                                                               guint                     height);
gint             ogmrip_options_dialog_get_crop               (OGMRipOptionsDialog       *dialog, 
                                                               guint                     *x, 
                                                               guint                     *y, 
                                                               guint                     *width, 
                                                               guint                     *height);
void             ogmrip_options_dialog_set_crop               (OGMRipOptionsDialog       *dialog,
                                                               OGMRipOptionsType         type,
                                                               guint                     x,
                                                               guint                     y,
                                                               guint                     width,
                                                               guint                     height);
gboolean         ogmrip_options_dialog_get_test               (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_set_test               (OGMRipOptionsDialog       *dialog,
                                                               gboolean                  test);
gint             ogmrip_options_dialog_get_deinterlacer       (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_set_deinterlacer       (OGMRipOptionsDialog       *dialog,
                                                               gboolean                  deint);

gboolean         ogmrip_options_dialog_get_cartoon            (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_set_cartoon            (OGMRipOptionsDialog       *dialog,
                                                               gboolean                  cartoon);

gchar *          ogmrip_options_dialog_get_active_profile     (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_set_active_profile     (OGMRipOptionsDialog       *dialog,
                                                               const gchar               *profile);

void             ogmrip_options_dialog_clear_profiles         (OGMRipOptionsDialog       *dialog);
void             ogmrip_options_dialog_add_profile            (OGMRipOptionsDialog       *dialog,
                                                               const gchar               *profile,
                                                               const gchar               *name);
void             ogmrip_options_dialog_remove_profile         (OGMRipOptionsDialog       *dialog,
                                                               const gchar               *profile);
void             ogmrip_options_dialog_rename_profile         (OGMRipOptionsDialog       *dialog,
                                                               const gchar               *profile,
                                                               const gchar               *new_name);

G_END_DECLS

#endif /* __OGMRIP_OPTIONS_DIALOG_H__ */

