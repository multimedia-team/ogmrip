/* OGMRip - A DVD Encoder for GNOME
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __OGMRIP_GCONF_H__
#define __OGMRIP_GCONF_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define OGMRIP_GCONF_ROOT                    "/apps/ogmrip"

/*
 * Preference keys
 */

#define OGMRIP_GCONF_GENERAL                 OGMRIP_GCONF_ROOT "/general"

#define OGMRIP_GCONF_PROFILE                 "profile"
#define OGMRIP_GCONF_OUTPUT_DIR              "output_dir"
#define OGMRIP_GCONF_FILENAME                "filename"
#define OGMRIP_GCONF_PREF_AUDIO              "pref_audio"
#define OGMRIP_GCONF_PREF_SUBP               "pref_subp"
#define OGMRIP_GCONF_CHAPTER_LANG            "chapter_lang"

#define OGMRIP_GCONF_ADVANCED                OGMRIP_GCONF_ROOT "/advanced"

#define OGMRIP_GCONF_TMP_DIR                 "tmp_dir"
#define OGMRIP_GCONF_COPY_DVD                "copy_dvd"
#define OGMRIP_GCONF_AFTER_ENC               "after_enc"
#define OGMRIP_GCONF_KEEP_TMP                "keep_tmp"
#define OGMRIP_GCONF_LOG_OUTPUT              "log_output"
#define OGMRIP_GCONF_THREADS                 "threads"
#define OGMRIP_GCONF_AUTO_SUBP               "auto_subp"

#define OGMRIP_GCONF_PROFILES                OGMRIP_GCONF_ROOT "/profiles"

/*
 * Profile keys
 */

#define OGMRIP_GCONF_PROFILE_NAME            "name"

#define OGMRIP_GCONF_CONTAINER               "container"

#define OGMRIP_GCONF_CONTAINER_FORMAT        OGMRIP_GCONF_CONTAINER "/format"
#define OGMRIP_GCONF_CONTAINER_FOURCC        OGMRIP_GCONF_CONTAINER "/fourcc"
#define OGMRIP_GCONF_CONTAINER_TNUMBER       OGMRIP_GCONF_CONTAINER "/target_number"
#define OGMRIP_GCONF_CONTAINER_TSIZE         OGMRIP_GCONF_CONTAINER "/target_size"
#define OGMRIP_GCONF_CONTAINER_ENSURE_SYNC   OGMRIP_GCONF_CONTAINER "/ensure_sync"

#define OGMRIP_GCONF_VIDEO                   "video"

#define OGMRIP_GCONF_VIDEO_CODEC             OGMRIP_GCONF_VIDEO "/codec"
#define OGMRIP_GCONF_VIDEO_PASSES            OGMRIP_GCONF_VIDEO "/passes"
#define OGMRIP_GCONF_VIDEO_PRESET            OGMRIP_GCONF_VIDEO "/preset"
#define OGMRIP_GCONF_VIDEO_SCALER            OGMRIP_GCONF_VIDEO "/scaler"
#define OGMRIP_GCONF_VIDEO_DENOISE           OGMRIP_GCONF_VIDEO "/denoise"
#define OGMRIP_GCONF_VIDEO_TRELLIS           OGMRIP_GCONF_VIDEO "/trellis"
#define OGMRIP_GCONF_VIDEO_QPEL              OGMRIP_GCONF_VIDEO "/qpel"
#define OGMRIP_GCONF_VIDEO_DEBLOCK           OGMRIP_GCONF_VIDEO "/deblock"
#define OGMRIP_GCONF_VIDEO_DERING            OGMRIP_GCONF_VIDEO "/dering"
#define OGMRIP_GCONF_VIDEO_TURBO             OGMRIP_GCONF_VIDEO "/turbo"
#define OGMRIP_GCONF_VIDEO_ASPECT            OGMRIP_GCONF_VIDEO "/aspect_ratio"

#define OGMRIP_GCONF_VIDEO_ENCODING          OGMRIP_GCONF_VIDEO "/encoding"
#define OGMRIP_GCONF_VIDEO_BITRATE           OGMRIP_GCONF_VIDEO "/bitrate"
#define OGMRIP_GCONF_VIDEO_QUANTIZER         OGMRIP_GCONF_VIDEO "/quantizer"
#define OGMRIP_GCONF_VIDEO_BPP               OGMRIP_GCONF_VIDEO "/bpp"

#define OGMRIP_GCONF_VIDEO_CAN_CROP          OGMRIP_GCONF_VIDEO "/can_crop"
#define OGMRIP_GCONF_VIDEO_CAN_SCALE         OGMRIP_GCONF_VIDEO "/can_scale"
#define OGMRIP_GCONF_VIDEO_MIN_WIDTH         OGMRIP_GCONF_VIDEO "/min_width"
#define OGMRIP_GCONF_VIDEO_MIN_HEIGHT        OGMRIP_GCONF_VIDEO "/min_height"
#define OGMRIP_GCONF_VIDEO_MAX_WIDTH         OGMRIP_GCONF_VIDEO "/max_width"
#define OGMRIP_GCONF_VIDEO_MAX_HEIGHT        OGMRIP_GCONF_VIDEO "/max_height"
#define OGMRIP_GCONF_VIDEO_EXPAND            OGMRIP_GCONF_VIDEO "/expand"

#define OGMRIP_GCONF_AUDIO                   "audio"

#define OGMRIP_GCONF_AUDIO_CODEC             OGMRIP_GCONF_AUDIO "/codec"
#define OGMRIP_GCONF_AUDIO_QUALITY           OGMRIP_GCONF_AUDIO "/quality"
#define OGMRIP_GCONF_AUDIO_CHANNELS          OGMRIP_GCONF_AUDIO "/channels"
#define OGMRIP_GCONF_AUDIO_SRATE             OGMRIP_GCONF_AUDIO "/srate"
#define OGMRIP_GCONF_AUDIO_NORMALIZE         OGMRIP_GCONF_AUDIO "/normalize"

#define OGMRIP_GCONF_SUBP                    "subp"

#define OGMRIP_GCONF_SUBP_CODEC              OGMRIP_GCONF_SUBP "/codec"
#define OGMRIP_GCONF_SUBP_CHARSET            OGMRIP_GCONF_SUBP "/charset"
#define OGMRIP_GCONF_SUBP_NEWLINE            OGMRIP_GCONF_SUBP "/newline"
#define OGMRIP_GCONF_FORCED_SUBS             OGMRIP_GCONF_SUBP "/forced"
#define OGMRIP_GCONF_SPELL_CHECK             OGMRIP_GCONF_SUBP "/spell_check"

/*
 * Default preferences
 */

#define OGMRIP_DEFAULT_PROFILE               "default-ogm"
#define OGMRIP_DEFAULT_OUTPUT_DIR            g_get_home_dir ()
#define OGMRIP_DEFAULT_FILENAME              0
#define OGMRIP_DEFAULT_PREF_AUDIO            0
#define OGMRIP_DEFAULT_PREF_SUBP             0
#define OGMRIP_DEFAULT_CHAPTER_LANG          0

#define OGMRIP_DEFAULT_TMP_DIR               ogmrip_fs_get_tmp_dir ()
#define OGMRIP_DEFAULT_COPY_DVD              FALSE
#define OGMRIP_DEFAULT_AFTER_ENC             0
#define OGMRIP_DEFAULT_KEEP_TMP              FALSE
#define OGMRIP_DEFAULT_LOG_OUTPUT            FALSE
#define OGMRIP_DEFAULT_THREADS               1
#define OGMRIP_DEFAULT_AUTO_SUBP             TRUE

#define OGMRIP_DEFAULT_CONTAINER_FORMAT      "ogm"
#define OGMRIP_DEFAULT_CONTAINER_FOURCC      0
#define OGMRIP_DEFAULT_CONTAINER_TNUMBER     1
#define OGMRIP_DEFAULT_CONTAINER_TSIZE       700
#define OGMRIP_DEFAULT_CONTAINER_ENSURE_SYNC TRUE

#define OGMRIP_DEFAULT_VIDEO_CODEC           "lavc-mpeg4"
#define OGMRIP_DEFAULT_VIDEO_PASSES          2
#define OGMRIP_DEFAULT_VIDEO_PRESET          0
#define OGMRIP_DEFAULT_VIDEO_SCALER          7
#define OGMRIP_DEFAULT_VIDEO_DENOISE         TRUE
#define OGMRIP_DEFAULT_VIDEO_TRELLIS         TRUE
#define OGMRIP_DEFAULT_VIDEO_QPEL            FALSE
#define OGMRIP_DEFAULT_VIDEO_DEBLOCK         FALSE
#define OGMRIP_DEFAULT_VIDEO_DERING          FALSE
#define OGMRIP_DEFAULT_VIDEO_TURBO           TRUE
#define OGMRIP_DEFAULT_VIDEO_ASPECT          2

#define OGMRIP_DEFAULT_VIDEO_ENCODING        0
#define OGMRIP_DEFAULT_VIDEO_BITRATE         800
#define OGMRIP_DEFAULT_VIDEO_QUANTIZER       2
#define OGMRIP_DEFAULT_VIDEO_BPP             0.25

#define OGMRIP_DEFAULT_VIDEO_CAN_CROP        TRUE
#define OGMRIP_DEFAULT_VIDEO_CAN_SCALE       TRUE
#define OGMRIP_DEFAULT_VIDEO_MIN_WIDTH       0
#define OGMRIP_DEFAULT_VIDEO_MIN_HEIGHT      0
#define OGMRIP_DEFAULT_VIDEO_MAX_WIDTH       0
#define OGMRIP_DEFAULT_VIDEO_MAX_HEIGHT      0
#define OGMRIP_DEFAULT_VIDEO_EXPAND          FALSE

#define OGMRIP_DEFAULT_AUDIO_CODEC           "vorbis"
#define OGMRIP_DEFAULT_AUDIO_QUALITY         3
#define OGMRIP_DEFAULT_AUDIO_CHANNELS        1
#define OGMRIP_DEFAULT_AUDIO_SRATE           0
#define OGMRIP_DEFAULT_AUDIO_NORMALIZE       TRUE

#define OGMRIP_DEFAULT_SUBP_CODEC            "vobsub"
#define OGMRIP_DEFAULT_SUBP_CHARSET          0
#define OGMRIP_DEFAULT_SUBP_NEWLINE          0
#define OGMRIP_DEFAULT_FORCED_SUBS           FALSE
#define OGMRIP_DEFAULT_SPELL_CHECK           FALSE

enum
{
  OGMRIP_AFTER_ENC_REMOVE,
  OGMRIP_AFTER_ENC_KEEP,
  OGMRIP_AFTER_ENC_UPDATE,
  OGMRIP_AFTER_ENC_ASK
};

void  ogmrip_gconf_init   (void);
void  ogmrip_gconf_uninit (void);

GType ogmrip_gconf_get_container_type   (const gchar *section,
                                         const gchar *name);
GType ogmrip_gconf_get_video_codec_type (const gchar *section,
                                         const gchar *name);
GType ogmrip_gconf_get_audio_codec_type (const gchar *section,
                                         const gchar *name);
GType ogmrip_gconf_get_subp_codec_type  (const gchar *section,
                                         const gchar *name);

G_END_DECLS

#endif /* __OGMRIP_GCONF_H__ */

